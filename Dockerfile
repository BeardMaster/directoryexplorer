FROM openjdk:8

# Get Maven
RUN apt-get update && apt-get install -y maven

WORKDIR /usr/directoryExplorer/

# Get dependancies
ADD pom.xml /usr/directoryExplorer/pom.xml
RUN ["mvn", "dependency:resolve"]
RUN ["mvn", "verify"]

ADD src /usr/directoryExplorer/src
RUN ["mvn", "package"]

EXPOSE 4567
CMD ["/usr/lib/jvm/java-8-openjdk-amd64/bin/java","-jar","target/directoryexplorer-jar-with-dependencies.jar"]