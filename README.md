# README #

## What is DirectoryExplorer? ##

Directory explorer is a small REST API that can run within a docker container on a server and allows clients to get a full directory listing of a given server directory, including; full path names, file size and attributes and whether the given directory item is a sub-directory (dir) or a file (file).


## Prerequisites for a machine to be able to run this REST API ##

* Docker / Docker Toolbox (with Virtual Box installed)
* GIT

## Steps to get it running ##

1) First clone the source code (including Dockerfile) to a local directory:

```
git clone https://BeardMaster@bitbucket.org/BeardMaster/directoryexplorer.git
```

2) Run the following docker commands to get the REST API up and running.

Open command prompt at the location where the git repo was cloned (where the docker file is also present).

#### Build ####
**Please note:** This command will run an update of the base system the first time which can take a while depending on internet connection.
```
docker build -t directoryexplorer .
```

#### Run ####
```
docker run -d -p 8080:4567 --name direxp directoryexplorer
```

#### Test ####
Hit the following URL:
```
http://DOCKER_MACHINE_IP:8080/explorer?directory=/
```

## Using the API ##

1. All requests spawn from the /explorer/ path.
2. Only GET requests are supported at this stage.
3. Querying for a specific directory listing is done by means of a **directory** query parameter:

```
http://DOCKER_MACHINE_IP:8080/explorer?directory=/usr
```

##### Server side paging support #####
Server side paging is available for large directories using the **page** query parameter. (The page size is set to 50 items per page.)

Return a specific page of items:
```
http://DOCKER_MACHINE_IP:8080/explorer?directory=/&page=1
```

OR **page** query parameter can be left out or set to **all** to return full directory listing

```
http://DOCKER_MACHINE_IP:8080/explorer?directory=/usr&page=all

http://DOCKER_MACHINE_IP:8080/explorer?directory=/usr

```

##### Error messages #####

When searching a directory that does not exist you will receive a 404 HTTP status with the following message:

```
{"message":"Directory not found."}
```

When searching a directory that is empty you will get a 200 HTTP status with the following message:

```
{"message":"Empty directory."}
```