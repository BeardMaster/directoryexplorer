package com.phillip.app;

import spark.ResponseTransformer;
import com.google.gson.Gson;

public class JSONTransformer implements ResponseTransformer
{
	private Gson gson = new Gson();

	@Override
	public String render(Object obj) throws Exception
	{
		return gson.toJson(obj);
	}

}
