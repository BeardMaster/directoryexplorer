package com.phillip.app.controllers;

import static spark.Spark.get;

import com.phillip.app.JSONTransformer;
import com.phillip.app.response.DirectoryListingResponse;
import com.phillip.app.response.ErrorResponse;
import com.phillip.app.services.DirectoryListingService;

public class DirectoryListingController
{
	public DirectoryListingController(DirectoryListingService directoryListingService)
	{
		get("/explorer", "application/json", (req, res) -> {
			res.header("Content-Encoding", "gzip");

			DirectoryListingResponse dirList = directoryListingService.getDirectoryListing(req.queryParams("directory"),
					getPage(req.queryParams("page")));

			if (dirList == null)
			{
				res.status(404);
				return new ErrorResponse("Directory not found.");
			}

			if (dirList.getDirectoryDetailList() == null)
			{
				return new ErrorResponse("Empty directory.");
			}

			return dirList;
		}, new JSONTransformer());
	}

	private int getPage(String page)
	{
		if (page == null || page.equals("all"))
		{
			return -1;
		} else
		{
			return Integer.parseInt(page) - 1;
		}
	}

}
