package com.phillip.app.response;

public class FileAttributeInformation
{
	private String createTime;
	private String modifiedTime;
	private String accessedTime;

	public FileAttributeInformation(String creationTime, String lastModifiedTime, String lastAccessTime)
	{
		this.createTime = creationTime;
		this.modifiedTime = lastModifiedTime;
		this.accessedTime = lastAccessTime;
	}

	public String getCreationTime()
	{
		return createTime;
	}

	public void setCreationTime(String creationTime)
	{
		this.createTime = creationTime;
	}

	public String getLastModifiedTime()
	{
		return modifiedTime;
	}

	public void setLastModifiedTime(String lastModifiedTime)
	{
		this.modifiedTime = lastModifiedTime;
	}

	public String getLastAccessTime()
	{
		return accessedTime;
	}

	public void setLastAccessTime(String lastAccessTime)
	{
		this.accessedTime = lastAccessTime;
	}

}
