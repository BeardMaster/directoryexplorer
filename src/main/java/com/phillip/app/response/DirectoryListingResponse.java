package com.phillip.app.response;

import java.util.ArrayList;
import java.util.List;

public class DirectoryListingResponse
{
	private List<DirectoryListingDetail> fileList;
	private int totalPages;
	private int currentPage;

	public int getCurrentPage()
	{
		return currentPage;
	}

	public void setCurrentPage(int currentPage)
	{
		this.currentPage = currentPage;
	}

	public DirectoryListingResponse()
	{
	}

	public List<DirectoryListingDetail> getDirectoryDetailList()
	{
		return fileList;
	}

	public void setDirectoryDetailList(List<DirectoryListingDetail> directoryDetailList)
	{
		this.fileList = directoryDetailList;
	}

	public void addDirectoryDetailToList(DirectoryListingDetail directoryDetail)
	{
		if (fileList == null)
		{
			fileList = new ArrayList<DirectoryListingDetail>();
		}

		this.fileList.add(directoryDetail);
	}

	public int getTotalPages()
	{
		return totalPages;
	}

	public void setTotalPages(int totalPages)
	{
		this.totalPages = totalPages;
	}

}
