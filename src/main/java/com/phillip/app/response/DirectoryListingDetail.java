package com.phillip.app.response;

public class DirectoryListingDetail
{
	private String path;
	private long size;
	private FileAttributeInformation attrs;
	private String type;

	public DirectoryListingDetail()
	{

	}

	public DirectoryListingDetail(String fullFilePath, long fileSize, FileAttributeInformation attributeInformation,
			String type)
	{
		this.path = fullFilePath;
		this.size = fileSize;
		this.attrs = attributeInformation;
		this.type = type;
	}

	public String getFullFilePath()
	{
		return path;
	}

	public void setFullFilePath(String fullFilePath)
	{
		this.path = fullFilePath;
	}

	public long getFileSize()
	{
		return size;
	}

	public void setFileSize(long fileSize)
	{
		this.size = fileSize;
	}

	public FileAttributeInformation getAttributeInformation()
	{
		return attrs;
	}

	public void setAttributeInformation(FileAttributeInformation attributeInformation)
	{
		this.attrs = attributeInformation;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}
}
