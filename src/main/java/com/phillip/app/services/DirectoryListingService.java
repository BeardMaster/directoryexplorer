package com.phillip.app.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

import com.phillip.app.response.DirectoryListingDetail;
import com.phillip.app.response.DirectoryListingResponse;
import com.phillip.app.response.FileAttributeInformation;

public class DirectoryListingService
{
	private final int PAGE_SIZE = 50;
	private String[] files;
	private DirectoryListingResponse dirList = null;
	String absolutePath;

	public DirectoryListingResponse getDirectoryListing(String directory, int page) throws IOException
	{
		File mainFileObject = new File(directory);
		dirList = null;

		if (mainFileObject.exists())
		{
			initializeGlobals(mainFileObject);

			if (isNotPaged(page))
			{
				getFullDirectoryListing();
			} else
			{
				getPagedDirectoryListing(page);
			}
		}

		return dirList;
	}

	private void initializeGlobals(File mainFileObject) throws IOException
	{
		dirList = new DirectoryListingResponse();
		absolutePath = mainFileObject.getCanonicalPath();
		files = mainFileObject.list();
		dirList.setTotalPages((int) Math.ceil((files.length / (float) PAGE_SIZE)));
	}

	private boolean isNotPaged(int page)
	{
		return page == -1;
	}

	private void getPagedDirectoryListing(int page) throws IOException
	{
		dirList.setCurrentPage(page + 1);
		for (int i = (PAGE_SIZE * page); i < ((PAGE_SIZE * page) + PAGE_SIZE); i++)
		{
			if (i < files.length)
			{
				String fullPath = absolutePath + File.separator + files[i];
				BasicFileAttributes attr = getFileAttributes(fullPath);
				addItemToDirectoryListing(fullPath, attr);
			} else
			{
				break;
			}
		}
	}

	private BasicFileAttributes getFileAttributes(String fullPath) throws IOException
	{
		Path path = Paths.get(fullPath);
		BasicFileAttributes attr = Files.readAttributes(path, BasicFileAttributes.class);
		return attr;
	}

	private void addItemToDirectoryListing(String fullPath, BasicFileAttributes attr)
	{
		dirList.addDirectoryDetailToList(new DirectoryListingDetail(fullPath, attr.size(),
				makeFileAttributeObject(attr), getType(attr.isDirectory())));
	}

	private FileAttributeInformation makeFileAttributeObject(BasicFileAttributes attr)
	{
		return new FileAttributeInformation(attr.creationTime().toString(), attr.lastModifiedTime().toString(),
				attr.lastAccessTime().toString());
	}

	private String getType(boolean isDirectory)
	{
		return (isDirectory) ? "dir" : "file";
	}

	private void getFullDirectoryListing() throws IOException
	{
		dirList.setCurrentPage(1);
		for (String file : files)
		{
			String fullPath = absolutePath + File.separator + file;
			BasicFileAttributes attr = getFileAttributes(fullPath);
			addItemToDirectoryListing(fullPath, attr);
		}
	}
}
