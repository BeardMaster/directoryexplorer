package com.phillip.app;

import static spark.Spark.before;
import static spark.Spark.options;

import java.io.IOException;

import org.apache.log4j.BasicConfigurator;

import com.phillip.app.controllers.DirectoryListingController;
import com.phillip.app.services.DirectoryListingService;

public class App
{
	private final static String CORS_ALLOW_ORIGIN = "*";
	private final static String CORS_REQUEST_METHODS = "POST, GET, OPTIONS, DELETE, PUT";
	private final static String CORS_ALLOW_HEADERS = "X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding";

	public static void main(String[] args) throws IOException
	{
		BasicConfigurator.configure();
		enableCORS(CORS_ALLOW_ORIGIN, CORS_REQUEST_METHODS, CORS_ALLOW_HEADERS);

		new DirectoryListingController(new DirectoryListingService());
	}

	// Enables CORS on requests.
	private static void enableCORS(final String origin, final String methods, final String headers)
	{

		options("/*", (request, response) -> {

			String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
			if (accessControlRequestHeaders != null)
			{
				response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
			}

			String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
			if (accessControlRequestMethod != null)
			{
				response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
			}

			return "OK";
		});

		before((request, response) -> {
			response.header("Access-Control-Allow-Origin", origin);
			response.header("Access-Control-Request-Method", methods);
			response.header("Access-Control-Allow-Headers", headers);
		});
	}

}
